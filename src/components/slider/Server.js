/* eslint class-methods-use-this: 0 */

class Server {
  fetchImages(url) {
    return window
      .fetch(url)
      .then(response => response.json());
  }

  preloadImage(url) {
    return new Promise((resolve) => {
      const img = new Image();
      img.onload = resolve;
      img.src = url;
    });
  }
}

export default new Server();
