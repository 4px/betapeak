# betapeak

A simple project created by vue cli ui, containing `eslint-airbnb` (_recommended_ setup), `test-utils` working with `mocha` and `chai`, along with `sinon` for fakes.
Also working with `scss`.

A few things to keep in mind:

- There is zero support fo aria and usability as a whole. Typically it's something to be considered, but I didn't spend any time on it, thinking "it's good enough" this way.
- Components are not broken down into very tiny pieces, again just because I think that's good enough and there is no need of empty components with zero logic in them.
- I've decided to use `transition-group` approach, as more than one element needs to be visible at the same time. Another good way to solve it is to have the entire strip at once, but hidden, but this requires moving objects separatedly and adding/removing classes for animation and handling all events. I decided it's faster this way and the result would be much the same. The only "issue" is that images are not automatically preloaded, but I've included a sequential simple preload method for that (not very clever).
- The result is not bulletproof - there are plenty of places where error handling can be done (server fetching, data parsing, state management). I don't feel there's a need to handle edge cases about a fairly simple component. It can be polished forever, but I hope that's not what you're looking for. Clicking arrows too fast messes up the result. Switching images too early shows blank page and loading image.
- I've added 800ms delay in `mocky.io` so it simulates a real server loading.
- Testint turned out to be my weakest part. I've spend the most time there, finding I'm a bit behind the last trends. I've spent half an hour clarifying I should not use `shallowMount` :D I've added a simple spec file. There's not much to test, but I've tried to write at least a few. Moved some pieces of the code so that they can be mocked by `sinon`. Honestly I grew so frustrated with all the little problems I had (mostly mocking), that I decided do ditch cypress for e2e. I've added simple suits close to that in the default one - things like loading, simulating clicking and expecting the result of that. I hope that's good enough as I am a fast learned, but I realized I've spend too much time in this, and my knowledge is a bit outdated and maybe now is not the best time to refresh it :)
- I don't know if `setup.js` is needed for tests, nor what's up with `jsdom-global`. Currently don't rely on that.
- I've never built the entire project - hopefully there's nothing wrong happening.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```
