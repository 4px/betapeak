import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import ImageSlider from '@/components/ImageSlider.vue';
import sinon from 'sinon';
import Server from '../../src/components/slider/Server';

describe('ImageSlider.vue', () => {
  sinon.replace(Server, 'fetchImages', sinon.fake.resolves({
    images: [
      'https://placeimg.com/640/480/arch',
      'https://placeimg.com/640/480/animals',
      'https://placeimg.com/640/480/nature'
    ]
  }));

  sinon.replace(Server, 'preloadImage', sinon.fake.resolves());

  const wrapper = mount(ImageSlider, {
    propsData: {
      width: 500,
      height: 300
    }
  });


  describe('on init', () => {
    it('has "width" set to 500', () => {
      expect(wrapper.vm).to.have.property('width');
      expect(wrapper.vm.width).to.equal(500);
    });

    it('has "height" set to 300', () => {
      expect(wrapper.vm).to.have.property('height');
      expect(wrapper.vm.height).to.equal(300);
    });
  });


  describe('on data loaded', () => {
    it('loading === false', () => {
      expect(wrapper.vm.loading).to.equal(false); // loading data is mocked, so it's instant :)
    });
    it('allImages.length === 3', () => {
      expect(wrapper.vm.allImages).to.have.lengthOf(3);
    });

    it('first image to be done loading', () => {
      expect(wrapper.emitted()).to.have.property('show'); // emitted when first image is loaded
    });

    it('images list to be visible', () => {
      expect(wrapper.find('.images').isVisible()).to.equal(true);
    });
  });


  describe('on simulating clicks', () => {
    it('on next click => currentIndex === 1', () => {
      wrapper.find('.next').trigger('click');
      expect(wrapper.vm.currentIndex).to.equal(1);
    });

    it('on second next click -> currentIndex === 2', () => {
      wrapper.find('.next').trigger('click');
      expect(wrapper.vm.currentIndex).to.equal(2);
    });

    it('current url should be nature', () => {
      const image = wrapper.find('.image').attributes().style;
      expect(image).to.include('nature');
    });

    it('on prev click => currentIndex === 1', () => {
      wrapper.find('.prev').trigger('click');
      expect(wrapper.vm.currentIndex).to.equal(1);
    });
  });
});
